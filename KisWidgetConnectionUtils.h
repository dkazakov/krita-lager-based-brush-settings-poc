/*
 *  SPDX-FileCopyrightText: 2022 Dmitry Kazakov <dimula73@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef KISWIDGETCONNECTIONUTILS_H
#define KISWIDGETCONNECTIONUTILS_H

#include <QMetaType>
#include <QStringList>

class QAbstractButton;
class QComboBox;
class QButtonGroup;
class QSpinBox;
class QDoubleSpinBox;
class QObject;

namespace KisWidgetConnectionUtils {

template<typename T>
struct ControlState {
    T value = T{};
    bool enabled = true;
};

struct ToControlState {
    template<typename T>
    ControlState<std::decay_t<T>> operator()(T &&value, bool enabled) {
        return {std::forward<T>(value), enabled};
    }
};

using CheckBoxState = ControlState<bool>;

struct ComboBoxState {
    QStringList items;
    int currentIndex = -1;
    bool enabled = true;
};

void connectControl(QAbstractButton *button, QObject *source, const char *property);
void connectControl(QSpinBox *spinBox, QObject *source, const char *property);
void connectControl(QDoubleSpinBox *spinBox, QObject *source, const char *property);
void connectControl(QButtonGroup *button, QObject *source, const char *property);
void connectControlState(QAbstractButton *button, QObject *source, const char *readStateProperty, const char *writeProperty);
void connectControlState(QComboBox *button, QObject *source, const char *readStateProperty, const char *writeProperty);

};

using KisWidgetConnectionUtils::CheckBoxState;
using KisWidgetConnectionUtils::ComboBoxState;

Q_DECLARE_METATYPE(CheckBoxState)
Q_DECLARE_METATYPE(ComboBoxState)

#endif // KISWIDGETCONNECTIONUTILS_H
