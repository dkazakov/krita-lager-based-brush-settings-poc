/*
 *  SPDX-FileCopyrightText: 2022 Dmitry Kazakov <dimula73@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef BRUSHCONFIG_H
#define BRUSHCONFIG_H

#include <QtGlobal>

//#define USE_CEREAL

#ifdef USE_CEREAL
#include <lager/debug/cereal/immer_flex_vector.hpp>
#include <lager/debug/cereal/struct.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/cereal.hpp>
#endif

namespace BrushEngine {

struct AutoBrushConfig {
    int spikes = 2;
    qreal ratio = 1.0;
};

struct PredefinedBrushConfig {
    bool hasColorData = false;
    bool supportsLightnessMode = false;
    enum BrushMode {
        Mask,
        Image,
        Lightness
    };

    BrushMode mode = Mask;
};

struct SmearConfig {
    bool smearAlpha = true;
    bool useNewBlendingAlgorithm = false;
};

struct PaintThicknessConfig {
    bool usePaintThickness = false;
};

struct BrushConfig {
    AutoBrushConfig autoBrushConfig;
    PredefinedBrushConfig predefinedBrushConfig;
    int brushType = 0;
};

struct ColorSmudgePresetConfig {
    BrushConfig brush;
    SmearConfig smear;
    PaintThicknessConfig paintThickness;
};

#ifdef USE_CEREAL
LAGER_CEREAL_STRUCT(BrushEngine::AutoBrushConfig, (spikes)(ratio));
LAGER_CEREAL_STRUCT(BrushEngine::PredefinedBrushConfig, (hasColorData)(supportsLightnessMode)(mode));
LAGER_CEREAL_STRUCT(BrushEngine::SmearConfig, (smearAlpha)(useNewBlendingAlgorithm));
LAGER_CEREAL_STRUCT(BrushEngine::PaintThicknessConfig, (usePaintThickness));
LAGER_CEREAL_STRUCT(BrushEngine::BrushConfig, (autoBrushConfig)(predefinedBrushConfig)(brushType));
LAGER_CEREAL_STRUCT(BrushEngine::ColorSmudgePresetConfig, (brush)(smear)(paintThickness));
#endif
}

#endif // BRUSHCONFIG_H
