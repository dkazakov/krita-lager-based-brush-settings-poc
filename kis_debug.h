/*
 *  SPDX-FileCopyrightText: 2022 Dmitry Kazakov <dimula73@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef KIS_DEBUG_H
#define KIS_DEBUG_H

#include <QDebug>

#define ppVar( var ) #var << "=" << (var)

#ifdef __GNUC__
QString __methodName(const char *prettyFunction);
#define __METHOD_NAME__ __methodName(__PRETTY_FUNCTION__)
#else
#define __METHOD_NAME__ "<unknown>:<unknown>"
#endif

#define PREPEND_METHOD(msg) QString("%1: %2").arg(__METHOD_NAME__).arg(msg)

#ifdef __GNUC__
#define ENTER_FUNCTION() qDebug() << "Entering" << __METHOD_NAME__
#define LEAVE_FUNCTION() qDebug() << "Leaving " << __METHOD_NAME__
#else
#define ENTER_FUNCTION() qDebug() << "Entering" << "<unknown>"
#define LEAVE_FUNCTION() qDebug() << "Leaving " << "<unknown>"
#endif

#define KIS_ASSERT(cond) if (!(cond)) qFatal("ASSERT: \"%s\" in file %s, line %d", #cond, __FILE__, __LINE__)
#define KIS_SAFE_ASSERT_RECOVER_RETURN KIS_ASSERT
#define KIS_SAFE_ASSERT_RECOVER_NOOP KIS_ASSERT


#endif // KIS_DEBUG_H
