/*
 *  SPDX-FileCopyrightText: 2022 Dmitry Kazakov <dimula73@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "MainWindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#include "./ui_MainWindow.h"

#include <QDebug>
#include <sstream>

#include "BrushConfig.h"

#include <lager/cursor.hpp>
#include <lager/extra/qt.hpp>
#include <lager/state.hpp>

#include <QStringList>
#include <tuple>

#include <kis_debug.h>
#include <KisWidgetConnectionUtils.h>

using KisWidgetConnectionUtils::ToControlState;

class AutoBrushModel : public QObject
{
    Q_OBJECT
public:
    AutoBrushModel(lager::cursor<BrushEngine::AutoBrushConfig> source)
        : m_source(source),
          LAGER_QT(spikes){m_source[&BrushEngine::AutoBrushConfig::spikes]},
          LAGER_QT(ratio){m_source[&BrushEngine::AutoBrushConfig::ratio]}
    {}

    lager::cursor<BrushEngine::AutoBrushConfig> m_source;

    LAGER_QT_CURSOR(int, spikes);
    LAGER_QT_CURSOR(qreal, ratio);
};

class PredefinedBrushModel : public QObject
{
    Q_OBJECT
public:
    PredefinedBrushModel(lager::cursor<BrushEngine::PredefinedBrushConfig> source)
        : m_source(source),
          LAGER_QT(hasColorData){m_source[&BrushEngine::PredefinedBrushConfig::hasColorData]},
          LAGER_QT(supportsLightnessMode){m_source[&BrushEngine::PredefinedBrushConfig::supportsLightnessMode]},
          LAGER_QT(mode){m_source[&BrushEngine::PredefinedBrushConfig::mode].xform(
                    zug::map([](auto&& x) { return static_cast<int>(x); }),
                    zug::map([](auto&& x) { return static_cast<BrushEngine::PredefinedBrushConfig::BrushMode>(x); }))},

          LAGER_QT(modeSwitchState){lager::with(LAGER_QT(hasColorData),
                                                LAGER_QT(supportsLightnessMode),
                                                LAGER_QT(mode)).xform(
                          zug::map([] (bool hasColorData, bool supportsLightnessMode, int mode) {
                  QStringList values;
                  values << "Mask";
                  if (hasColorData) {
                      values << "Image";
                      if (supportsLightnessMode) {
                          values << "Lightness";
                      }
                  }
                  int currentValue = std::clamp(mode, 0, values.size() - 1);
                  return ComboBoxState{values, currentValue, values.size() > 1};

              }))},
          LAGER_QT(lightnessModeEnabled) {LAGER_QT(modeSwitchState).xform(
                          zug::map([] (const ComboBoxState &state) {
                  return state.currentIndex == 2;
              }))}
    {}

    // the state must be declared **before** any cursors or readers
    lager::cursor<BrushEngine::PredefinedBrushConfig> m_source;

    LAGER_QT_READER(bool, hasColorData);
    LAGER_QT_READER(bool, supportsLightnessMode);
    LAGER_QT_CURSOR(int, mode);
    LAGER_QT_READER(ComboBoxState, modeSwitchState);
    LAGER_QT_READER(bool, lightnessModeEnabled);
};

class BrushModel : public QObject
{
    Q_OBJECT
public:
    BrushModel(lager::cursor<BrushEngine::BrushConfig> source)
        : m_source(source),
          LAGER_QT(brushType) {m_source[&BrushEngine::BrushConfig::brushType]},
          LAGER_QT(autoBrush){m_source[&BrushEngine::BrushConfig::autoBrushConfig]},
          LAGER_QT(predefinedBrush){m_source[&BrushEngine::BrushConfig::predefinedBrushConfig]}
        {}

    // the state must be declared **before** any cursors or readers
    lager::cursor<BrushEngine::BrushConfig> m_source;

    LAGER_QT_CURSOR(int, brushType);
    LAGER_QT_CURSOR(BrushEngine::AutoBrushConfig, autoBrush);
    LAGER_QT_CURSOR(BrushEngine::PredefinedBrushConfig, predefinedBrush);
};

class SmearModel : public QObject
{
    Q_OBJECT
public:
    SmearModel(lager::reader<int> brushType,
               lager::cursor<BrushEngine::PredefinedBrushConfig> predefinedBrushCursor,
               lager::cursor<BrushEngine::SmearConfig> smearCursor)
        : m_predefinedModel(predefinedBrushCursor),
          m_brushType(brushType),
          m_smearCursor(smearCursor),

    LAGER_QT(smearAlpha){m_smearCursor[&BrushEngine::SmearConfig::smearAlpha]},
    LAGER_QT(useNewBlendingAlgorithm){m_smearCursor[&BrushEngine::SmearConfig::useNewBlendingAlgorithm]},

    LAGER_QT(forceNewBlendingAlgorithm) {lager::with(
                    m_predefinedModel.LAGER_QT(modeSwitchState).xform(
                        zug::map([] (const ComboBoxState &state) {
                                     return state.currentIndex > 1;
                                 })),
                    m_brushType.xform(
                        zug::map([] (int brushType) {
                                     return brushType == 1;
                                 })))
                    .xform(zug::map(std::logical_and<>{}))},
    LAGER_QT(effectiveUseNewBlendingAlgorithm){lager::with(LAGER_QT(useNewBlendingAlgorithm),
                                                           LAGER_QT(forceNewBlendingAlgorithm)).xform(zug::map(std::logical_or<>{}))},
    LAGER_QT(newAlgorithmCheckState){lager::with(LAGER_QT(effectiveUseNewBlendingAlgorithm),
                                                 LAGER_QT(forceNewBlendingAlgorithm).xform(zug::map(std::logical_not<>{})))
                .xform(zug::map(ToControlState{}))}
    {}

    // the state must be declared **before** any cursors or readers

    PredefinedBrushModel m_predefinedModel;
    lager::reader<int> m_brushType;
    lager::cursor<BrushEngine::SmearConfig> m_smearCursor;


    LAGER_QT_CURSOR(bool, smearAlpha);
    LAGER_QT_CURSOR(bool, useNewBlendingAlgorithm);
    LAGER_QT_READER(bool, forceNewBlendingAlgorithm);
    LAGER_QT_READER(bool, effectiveUseNewBlendingAlgorithm);
    LAGER_QT_READER(CheckBoxState, newAlgorithmCheckState);
};

class PaintThicknessModel : public QObject
{
    Q_OBJECT
public:
    PaintThicknessModel(lager::reader<int> brushType,
                        lager::cursor<BrushEngine::PredefinedBrushConfig> predefinedBrushCursor,
                        lager::cursor<BrushEngine::PaintThicknessConfig> paintThicknessCursor)
        : m_predefinedModel(predefinedBrushCursor),
          m_brushType(brushType),
          m_paintThicknessCursor(paintThicknessCursor),
          LAGER_QT(usePaintThickness){m_paintThicknessCursor[&BrushEngine::PaintThicknessConfig::usePaintThickness]},
          LAGER_QT(paintThicknessEnabled){
              lager::with(m_brushType.xform(zug::map([] (int type) { return type == 1; } )),
                          m_predefinedModel.LAGER_QT(lightnessModeEnabled))
                      .xform(zug::map(std::logical_and<>{}))},
          LAGER_QT(effectiveUsePaintThinkness){
              lager::with(LAGER_QT(usePaintThickness),
                          LAGER_QT(paintThicknessEnabled))
                      .xform(zug::map(std::logical_and<>{}))},
          LAGER_QT(paintThinknessCheckState){
              lager::with(LAGER_QT(effectiveUsePaintThinkness),
                          LAGER_QT(paintThicknessEnabled))
                      .xform(zug::map(ToControlState{}))}
        {}

        // the state must be declared **before** any cursors or readers
    PredefinedBrushModel m_predefinedModel;
    lager::reader<int> m_brushType;
    lager::cursor<BrushEngine::PaintThicknessConfig> m_paintThicknessCursor;

    LAGER_QT_CURSOR(bool, usePaintThickness);
    LAGER_QT_READER(bool, paintThicknessEnabled);
    LAGER_QT_READER(bool, effectiveUsePaintThinkness);
    LAGER_QT_READER(CheckBoxState, paintThinknessCheckState);
};

struct MainWindow::Private {
    //BrushConfigQt config;
    lager::state<BrushEngine::ColorSmudgePresetConfig, lager::automatic_tag> state;

    BrushModel brushModel {state[&BrushEngine::ColorSmudgePresetConfig::brush]};
    AutoBrushModel autoBrushModel {brushModel.LAGER_QT(autoBrush)};
    PredefinedBrushModel predefinedBrushModel {brushModel.LAGER_QT(predefinedBrush)};
    SmearModel smearModel {
        brushModel.LAGER_QT(brushType),
        brushModel.LAGER_QT(predefinedBrush),
        state[&BrushEngine::ColorSmudgePresetConfig::smear]};
    PaintThicknessModel paintThicknessModel {
        brushModel.LAGER_QT(brushType),
        brushModel.LAGER_QT(predefinedBrush),
        state[&BrushEngine::ColorSmudgePresetConfig::paintThickness]};

    QScopedPointer<Ui::MainWindow> ui;

    void onStateChanged(const BrushEngine::ColorSmudgePresetConfig &config)  const {
        qDebug() << "state changed";

#ifdef USE_CEREAL
        std::stringstream stream;
        cereal::JSONOutputArchive archive(stream);
        archive(CEREAL_NVP(config));
        qDebug().noquote() << QString::fromStdString(stream.str()) << Qt::endl;
#endif

        ui->cmbSourceBrushMode->setCurrentIndex(static_cast<int>(config.brush.predefinedBrushConfig.mode));
        ui->chkSourceUsePaintThinkness->setChecked(config.paintThickness.usePaintThickness);
        ui->chkSourceSmearAlpha->setChecked(config.smear.smearAlpha);
        ui->chkSourceNewBlendingAlgorithm->setChecked(config.smear.useNewBlendingAlgorithm);
        ui->cmbSourceBrushType->setCurrentIndex(config.brush.brushType);
        ui->intSourceSpikes->setValue(config.brush.autoBrushConfig.spikes);
        ui->dblSourceRatio->setValue(config.brush.autoBrushConfig.ratio);
    }
};

#include <boost/bind/bind.hpp>

#include <QButtonGroup>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_d(new Private)
{
    using namespace KisWidgetConnectionUtils;

    m_d->ui.reset(new Ui::MainWindow);
    m_d->ui->setupUi(this);

    QButtonGroup *buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(m_d->ui->radioAutoBrush, 0);
    buttonGroup->addButton(m_d->ui->radioPredefinedBrush, 1);

    connect(m_d->ui->radioAutoBrush, &QAbstractButton::toggled, m_d->ui->grpAuto, &QGroupBox::setEnabled);
    connect(m_d->ui->radioPredefinedBrush, &QAbstractButton::toggled, m_d->ui->grpPredefined, &QGroupBox::setEnabled);

    connect(m_d->ui->btnWriteBrushConfig, &QPushButton::clicked, this, &MainWindow::slotWriteBrushConfig);
    m_d->onStateChanged(m_d->state.get());

    connect(&m_d->predefinedBrushModel, &PredefinedBrushModel::hasColorDataChanged, this, &MainWindow::slotUpdateHasColorData);
    slotUpdateHasColorData(m_d->predefinedBrushModel.hasColorData());

    connectControl(m_d->ui->chkSmearAlpha, &m_d->smearModel, "smearAlpha");
    connectControl(buttonGroup, &m_d->brushModel, "brushType");
    connectControlState(m_d->ui->chkNewBlendingAlgorithm, &m_d->smearModel, "newAlgorithmCheckState", "useNewBlendingAlgorithm");
    connectControlState(m_d->ui->chkUsePaintThinkness, &m_d->paintThicknessModel, "paintThinknessCheckState", "usePaintThickness");
    connectControlState(m_d->ui->cmbBrushMode, &m_d->predefinedBrushModel, "modeSwitchState", "mode");
    connectControl(m_d->ui->intSpikes, &m_d->autoBrushModel, "spikes");
    connectControl(m_d->ui->dblRatio, &m_d->autoBrushModel, "ratio");


    lager::watch(m_d->state, [this](const BrushEngine::ColorSmudgePresetConfig &config) {m_d->onStateChanged(config);});
}

MainWindow::~MainWindow()
{
}

void MainWindow::slotUpdateHasColorData(bool value)
{
    m_d->ui->lblHasColorData->setText(value ? "Yes" : "No");
}

void MainWindow::slotWriteBrushConfig()
{
    ENTER_FUNCTION();

    BrushEngine::ColorSmudgePresetConfig newConfig;

    newConfig.brush.predefinedBrushConfig.hasColorData = m_d->ui->chkSourceHasColorData->isChecked();
    newConfig.brush.predefinedBrushConfig.supportsLightnessMode = m_d->ui->chkSourceSupportsLightness->isChecked();
    newConfig.brush.predefinedBrushConfig.mode = static_cast<BrushEngine::PredefinedBrushConfig::BrushMode>(m_d->ui->cmbSourceBrushMode->currentIndex());
    newConfig.brush.brushType = m_d->ui->cmbSourceBrushType->currentIndex();
    newConfig.brush.autoBrushConfig.spikes = m_d->ui->intSourceSpikes->value();
    newConfig.brush.autoBrushConfig.ratio = m_d->ui->dblSourceRatio->value();
    newConfig.smear.smearAlpha = m_d->ui->chkSourceSmearAlpha->isChecked();
    newConfig.smear.useNewBlendingAlgorithm = m_d->ui->chkSourceNewBlendingAlgorithm->isChecked();
    newConfig.paintThickness.usePaintThickness = m_d->ui->chkSourceUsePaintThinkness->isChecked();

    m_d->state.set(newConfig);
}

#include "MainWindow.moc"
